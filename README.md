# React-translate
## Introduction
This is an simple implementation of translation provider for react apps.


## Install

  

```bash
yarn add @karsegard/react-translate

```

  

## Usage

see [example](example)
 

## License

  

MIT © [FDT2k](https://github.com/FDT2k)
