import React, { useState } from 'react';
import { Provider,useTranslation, withTranslation } from '@'



const ComponentThatNeedsLocalization = ({ t, children }) => {

    return (
        <h1>
            {t(children)}
        </h1>
    )
}



ComponentThatNeedsLocalization.defaultProps = {
    t: x => x
}


const TranslatableComponent = withTranslation('bia')(ComponentThatNeedsLocalization);


const async_translations = trans => _ => {
    return new Promise((resolve, reject) => {
        setTimeout(_ => {
            resolve(trans);
        }, 3000)
    })

}


const ComponentThatWaitForTranslation = props => {

    const {loading} = useTranslation();

    return (
        <> {loading && <h1>translation are still loading</h1>}
            <TranslatableComponent>hello_world</TranslatableComponent>
            <TranslatableComponent>this_key_has_no_fallback</TranslatableComponent>
        </>
    )

}

export default props => {
    const [lang, setLang] = useState('fr');

    const trans = {
        'bia': {
            'en': {
                'hello_world': 'Hello world'
            },
            'fr': {
                'hello_world': 'Bonjour monde',
                'this_key_has_no_fallback': 'Coucou'
            }
        }
    }
    console.log('rendered');

    return (

        <>
            <div>Hello world, let's test useLocalization</div>
            <select value={lang} onChange={e => setLang(e.target.value)}>
                <option value="fr">French</option>
                <option value="en">English</option>
                <option value="fdsfsd">Non existent lang</option>
                <option value="fdsfsd">Non existent lang and non existent key</option >
            </select>

            <Provider lang={lang} fallback="en" collect={true} translations={trans} >
                <TranslatableComponent>hello_world</TranslatableComponent>
                <TranslatableComponent>this_key_has_no_fallback</TranslatableComponent>
            </Provider>
            <div>testing with promise</div>


            <Provider lang={lang} fallback="en" collect={true} translations={async_translations(trans)}>
                <ComponentThatWaitForTranslation/>
            </Provider>

           
        </>
    )
}