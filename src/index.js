import React, { Children, createContext, useEffect, useMemo, useState, useContext } from 'react'
import { safe_path, is_type_function, is_type_string } from '@karsegard/composite-js'

const Context = createContext(null)



export { Context }


const getPath = (lang, namespace, key) => `${namespace}.${lang}.${key}`;

const translate = ({ lang, fallback, translations }) => namespace => (key, comment) => {


    const fb = safe_path(key, getPath(fallback, namespace, key), translations);

    const loc = safe_path(fb, getPath(lang, namespace, key), translations);
    console.log(getPath(fallback, namespace, key), translations)

    return loc;
}


const collectTranslation = ({ lang, fallback, translations }) => namespace => (key, comment) => {

    localStorage.setItem(`kda.rt.${namespace}.${fallback}.${key}`, JSON.stringify({ key, comment }));
    return translate({ lang, fallback, translations })(namespace)(key);
}

export const Provider = ({ lang, fallback, children, translations: _translations, collect = false }) => {

    let translator = collect ? collectTranslation : translate
    const [translations, setTranslations] = useState({});
    const [loading, setIsLoading] = useState(true);

    useEffect(() => {

        if (is_type_function(_translations)) {
            _translations().then(res => {
                setTranslations(res);
                setIsLoading(false);
            });
        } else {
            setIsLoading(false);
            setTranslations(_translations);
        }
    }, [_translations])


    const translate = { fallback, lang, translations };

    const provider = useMemo(_ => ({
        t: translator(translate),
        loading
    }), [loading]);


    return (
        <Context.Provider value={provider}>
            {children}
        </Context.Provider>
    )
}

export const withTranslation = namespace => Component => {
    return function LocalizedComponent(props) {
        return (
            <Context.Consumer>
                {args => {
                    const { t, ...context } = args;
                    return <Component
                        {...props}
                        {...context}
                        t={t(namespace)}
                    />;
                }}
            </Context.Consumer>
        );
    };
}



export const useTranslation = _ => useContext(Context);


export default {
    Context,
    Provider,
    withTranslation,
    useTranslation
}